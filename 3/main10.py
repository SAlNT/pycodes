import json
import tkinter as tk
from buttons import Button, ClearButton, ResultButton

# Get info from config

import re


class Button(object):

    def __init__(self, master, grid_coord, text, label_field, width, height):
        self.master = master
        self.grid_coord = grid_coord
        self.text = text
        self.button = tk.Button(master, text=text, command=self.btn_func,
                width=width, height=height, font='System 13 bold')
        self.button.grid(row=grid_coord[0], column=grid_coord[1])
        self.label_field = label_field

    def btn_func(self):
        if self.label_field["text"] == "ERROR" or\
                self.label_field["text"] == "":
            self.label_field["text"] = self.text            
        else:
            self.label_field["text"] += " " + self.text


class ClearButton(Button):

    def __init__(self, master, grid_coord, text, label_field, 
            width, height, num_symbols="one"):
        super().__init__(master, grid_coord, text, label_field, width, height)
        self.num_symbols = num_symbols
 
    def btn_func(self):
        if self.num_symbols == "one":
            self.label_field["text"] = self.label_field["text"][:-2]
        elif self.num_symbols == "all":
            self.label_field["text"] = ""


class ResultButton(Button):

    def btn_func(self):
        regex = r"[\d+*/().-]"
        string = re.findall(regex, self.label_field["text"])
        string = "".join(list(filter(None, string)))
        try:
            self.label_field["text"] = " ".join(str(eval(string)))
        except:
            self.label_field["text"] = "ERROR"


with open("config.json", "r") as f:
    config = json.load(f)
width = config["width"]
height = config["height"]

# Create window

root = tk.Tk()
root.title("Simple Calculator")
root.resizable(False, False)
label = tk.Label(text="", height=height*2, width=width*3, font='System 15 bold')
label.grid(row=0, column=0, columnspan=4)
buttons = []
for i in range(2, 5):
    for j in range(3):
        buttons.append(Button(master=root, grid_coord=[6-i, j],
                text=str(1 + j + 3 * (i - 2)), label_field=label, 
                width=width, height=height))

buttons.append(Button(root, [2, 3], "+", label, width, height))
buttons.append(Button(root, [3, 3], "-", label, width, height))
buttons.append(Button(root, [4, 3], "*", label, width, height))
buttons.append(Button(root, [5, 3], "/", label, width, height))
buttons.append(Button(root, [5, 0], ".", label, width, height))
buttons.append(Button(root, [5, 1], "0", label, width, height))
buttons.append(Button(root, [1, 2], "(", label, width, height))
buttons.append(Button(root, [1, 3], ")", label, width, height))

clear_all_btn = ClearButton(root, [1,0], "C", label, width, height, "all")
clear_one_btn = ClearButton(root, [1,1], "<-", label, width, height)

result_btn = ResultButton(root, [5, 2], "=", label, width, height)

root.mainloop()
